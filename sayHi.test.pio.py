import os

Import("env")

release_dir = os.path.join(env.GetLaunchDir(), "releases")


def build_task_chain(upstream_artefact):
    print(">>> Parsing tasks <<<")

    tag = env['PIOENV']

    release_file = env.Command(
        target=os.path.join(release_dir, tag + "_" + os.path.basename(upstream_artefact)),
        source=upstream_artefact,
        action=[
            Mkdir(release_dir),
            Copy("$TARGET", "$SOURCE"),
            sayHi
        ])

    release_descriptor = env.Command(
        target=os.path.join(release_dir, tag + ".json"),
        source=release_file,
        action=[
            Touch("$TARGET"),
            sayHi
        ])

    return release_descriptor


def sayHi(target, source, env):
    print(">>> Hi!  Finished creating %s from %s <<<" % (target[0], source[0]))


last_task_in_chain = build_task_chain("$BUILD_DIR/${PROGNAME}.bin");

script_action = env.AddCustomTarget(
    "my_target",
    [last_task_in_chain],
    []
)
